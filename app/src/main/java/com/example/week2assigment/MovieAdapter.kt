package com.example.week2assigment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.week2assigment.databinding.MovieRowLayoutBinding
import com.example.week2assigment.util.objects.Movie

class MovieAdapter(private val data: ArrayList<Movie>): RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MovieRowLayoutBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(data[position])
    }

    inner class ViewHolder(private val binding: MovieRowLayoutBinding): RecyclerView.ViewHolder(binding.root) {

        fun bindItems(item: Movie) {
            binding.item = item
            binding.executePendingBindings()
        }
    }
}