package com.example.week2assigment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.example.week2assigment.databinding.FragmentSearchMovieBinding
import com.example.week2assigment.util.JacksonUtils
import com.example.week2assigment.util.viewmodels.MoviesViewModel
import com.fasterxml.jackson.core.type.TypeReference

class SearchMovieFragment : Fragment(){

    private lateinit var manager: FragmentManager
    private lateinit var binding: FragmentSearchMovieBinding
    private lateinit var viewModel: MoviesViewModel
    private var suggestedList: ArrayList<String> = ArrayList(10)
    private var sharedPreferences: SharedPreferences? = null
    private lateinit var transaction: FragmentTransaction

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_movie, container, false)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE)
        val list = sharedPreferences?.getString(R.string.movies_shared_preferences_tag.toString(), "")
        if(!list.isNullOrEmpty()) {
            suggestedList = JacksonUtils()
                .fromJson(list, object : TypeReference<ArrayList<String>>() {})
        }
        activity?.let { viewModel = ViewModelProvider(it).get(MoviesViewModel::class.java) }
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        manager = parentFragmentManager
        binding.evSearchField.setOnTouchListener(object: View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when(event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        transaction = manager.beginTransaction()
                        transaction.add(R.id.suggestedList, SuggestedMoviesFragment()).commit()
                    }
                }
                return v?.onTouchEvent(event) ?: true
            }
        })
        binding.searchMovieBtn.setOnClickListener {
            if(viewModel.searchQuery.isNotEmpty()) {
                val movies = viewModel.getMovies(1).results
                if(!movies.isNullOrEmpty()) {
                    addToSharedPreferences()
                    val transaction = manager.beginTransaction()
                    transaction.replace(R.id.container, MoviesFragment()).addToBackStack(null).commit()
                } else {
                    binding.evSearchField.error = "No Movies found by this Search word"
                }
            } else {
                binding.evSearchField.error = "Please enter search word"
            }
        }
        return binding.root
    }

    private fun addToSharedPreferences() {
        suggestedList.add(viewModel.searchQuery)
        val newList = JacksonUtils().toJson(suggestedList)
        sharedPreferences?.edit()?.putString(R.string.movies_shared_preferences_tag.toString(), newList)?.apply()
    }
}
