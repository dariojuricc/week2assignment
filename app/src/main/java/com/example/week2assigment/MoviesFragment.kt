package com.example.week2assigment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.week2assigment.databinding.ActivityMoviesListBinding
import com.example.week2assigment.util.objects.Movie
import com.example.week2assigment.util.viewmodels.MoviesViewModel
import kotlinx.android.synthetic.main.activity_movies_list.*
import java.lang.Exception

class MoviesFragment: Fragment() {

    private lateinit var binding: ActivityMoviesListBinding
    private lateinit var recycler: RecyclerView
    private lateinit var vieww: View
    private lateinit var viewModel: MoviesViewModel
    private lateinit var layoutManager: LinearLayoutManager
    private var isLoading = true
    private var isLastPage = false
    private var page = 1
    lateinit var adapter: MovieAdapter
    private var moviesList = ArrayList<Movie>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_movies_list, container, false)
        vieww = binding.root
        return vieww
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler = binding.moviesList
        recycler.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this.activity)
        binding.moviesList.layoutManager = layoutManager
        activity?.let { viewModel = ViewModelProvider(it).get(MoviesViewModel::class.java) }
        getMoviesByName(page)
        scrollEfect()
    }

    private fun scrollEfect() {
        recycler.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val visibleItemCount = layoutManager.childCount
                val firstVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                val total = binding.moviesList.adapter?.itemCount
                if(!isLoading && !isLastPage) {
                    if((visibleItemCount + firstVisibleItem) >= total!!) {
                        page++
                        getMoviesByName(page)
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }

        })
    }

    private fun getMoviesByName(page: Int) {
        val totalPages = viewModel.getMovies(page).total_pages
        if(page == totalPages) {
            isLastPage = true
        }
        isLoading = true
        progressBar.visibility = View.VISIBLE
        val movies = viewModel.getMovies(page).results
        try{
            movies?.let { moviesList.addAll(it) }
        } catch (e: Exception) {
            Toast.makeText(this.context, "Something went wrong", Toast.LENGTH_LONG).show()
        }
        Handler().postDelayed( {
            if(::adapter.isInitialized) {
                adapter.notifyDataSetChanged()
            } else {
                adapter = MovieAdapter(moviesList)
                binding.moviesList.adapter = adapter
            }
            isLoading = false
            progressBar.visibility = View.GONE
        }, 2000)

    }
}
