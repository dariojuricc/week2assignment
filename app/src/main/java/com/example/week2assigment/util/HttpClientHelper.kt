package com.example.week2assigment.util

import okhttp3.*
import java.io.IOException

open class HttpClientHelper {

    private var client = OkHttpClient()
    
    @Throws(IOException::class)
    open fun get(url: HttpUrl): String? {
        val request: Request = Request.Builder()
            .url(url)
            .build()
        val response = client.newCall(request).execute()
        return response.body!!.string()
    }

    fun siteUrl(): HttpUrl {
        return HttpUrl.Builder().scheme("https").host("api.themoviedb.org").build()
    }
}