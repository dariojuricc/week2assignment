package com.example.week2assigment.util.objects

import com.fasterxml.jackson.annotation.JsonProperty

data class Results (
    @JsonProperty val page: Int? = null,
    @JsonProperty val total_pages: Int? = null,
    @JsonProperty val results: ArrayList<Movie>? = null
)