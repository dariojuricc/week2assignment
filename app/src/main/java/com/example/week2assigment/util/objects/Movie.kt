package com.example.week2assigment.util.objects

import com.fasterxml.jackson.annotation.JsonProperty

data class Movie (
    @JsonProperty val title: String? = null,
    @JsonProperty val release_date: String? = null,
    @JsonProperty val overview: String? = null
)