package com.example.week2assigment.util.models

import com.example.week2assigment.util.HttpClientHelper
import com.example.week2assigment.util.JacksonUtils
import com.example.week2assigment.util.objects.Results

class MoviesModel: HttpClientHelper() {

    fun getMovies(query: String?, page: Int): Results {
        val url = siteUrl().newBuilder()
            .addPathSegments("3/search/movie")
            .addQueryParameter("api_key", "2696829a81b1b5827d515ff121700838")
            .addEncodedQueryParameter("query", query)
            .addEncodedQueryParameter("page", page.toString())
            .build()
        val res = get(url)
        return JacksonUtils()
            .fromJson(res, Results::class.java)
    }
}