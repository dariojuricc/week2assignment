package com.example.week2assigment.util.viewmodels

import androidx.databinding.Bindable
import com.example.week2assigment.BR
import com.example.week2assigment.util.BaseAndroidViewModel
import com.example.week2assigment.util.models.MoviesModel
import com.example.week2assigment.util.objects.Results

class MoviesViewModel: BaseAndroidViewModel() {

    @get:Bindable
    var searchQuery: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.searchQuery)
        }

    fun getMovies(page: Int): Results {
        return MoviesModel().getMovies(searchQuery, page)
    }
}
