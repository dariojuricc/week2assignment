package com.example.week2assigment.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.kotlin.*;
import okhttp3.MediaType;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

public class JacksonUtils {
    private final ObjectMapper mapper;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    /**
     * Constructor.  Creates the Object Mapper.
     */
    public JacksonUtils() {
        mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).
                disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).
                registerModule(new JodaModule()).registerModule(new JavaTimeModule()).
                enable(SerializationFeature.INDENT_OUTPUT).registerModule(new KotlinModule());
    }

    /**
     * Returns JSON when given an object.
     *
     * @param sourceObject The object being serialized.
     * @return JSON.
     */
    public String toJson(Object sourceObject) {
        try {
            return mapper.writeValueAsString(sourceObject);
        } catch (JsonProcessingException ex) {
            System.out.println(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    /**
     * Gets object from Json.
     *
     * @param json     the JSON being converted.
     * @param classOfT The class type for the desired object.
     * @return the object from the JSON.
     */
    public <T> T fromJson(String json, Class<T> classOfT) {
        try {
            return mapper.readValue(json, classOfT);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    /**
     * Gets object from Json.
     *
     * @param json    the JSON being converted.
     * @param typeOfT The type reference for the desired object.
     * @return the object from the JSON.
     */
    public <T> T fromJson(String json, TypeReference<T> typeOfT) {
        try {
            return mapper.readValue(json, typeOfT);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    /**
     * Check if a field is annotated with an annotation.
     *
     * @param field          Field to check,
     * @param annotationType Annotation to check for. Example JsonIgnore.class.
     * @return True if field is annotated.
     */
    private static boolean hasAnnotation(Field field, Type annotationType) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        for (Annotation a : annotations) {
            if (a.annotationType() == annotationType) {
                return true;
            }
        }
        return false;
    }
}
