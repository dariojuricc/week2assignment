package com.example.week2assigment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import com.example.week2assigment.util.JacksonUtils
import com.example.week2assigment.util.viewmodels.MoviesViewModel
import com.fasterxml.jackson.core.type.TypeReference
import kotlinx.android.synthetic.main.fragment_suggested_movies.*

class SuggestedMoviesFragment : Fragment() {

    private lateinit var viewModel: MoviesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_suggested_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE)
        val list = sharedPreferences?.getString(R.string.movies_shared_preferences_tag.toString(), "")
        activity?.let { viewModel = ViewModelProvider(it).get(MoviesViewModel::class.java) }
        if(!list.isNullOrEmpty()) {
            val fromJson = JacksonUtils()
                .fromJson(list, object : TypeReference<ArrayList<String>>() {})
            val adapter = this.context?.let { ArrayAdapter(it, android.R.layout.simple_list_item_1, fromJson) }
            suggestedMovieList.adapter = adapter
        }
        suggestedMovieList.setOnItemClickListener { parent, view, position, id ->
            val item = suggestedMovieList.adapter.getItem(position)
            viewModel.searchQuery = item.toString()
            val transaction = parentFragmentManager.beginTransaction()
            transaction.replace(R.id.container, MoviesFragment()).addToBackStack(null).commit()
        }

        super.onViewCreated(view, savedInstanceState)
    }
}
